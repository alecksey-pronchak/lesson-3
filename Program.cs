﻿using System.Linq.Expressions;
using System.Xml.Linq;

internal class Program
{
    private static void Main(string[] args)
    {
        while (true) 
        {
            Console.Clear();

        float operand1, operand2;

        try
        {
            Console.WriteLine("Введите первое число: ");
                operand1 = float.Parse(Console.ReadLine());

            Console.WriteLine("Введите второе число: ");
                operand2 = float.Parse(Console.ReadLine());
        }

        catch (Exception)
        {
            Console.WriteLine("Ошибка ввода с клавиатуры");
            Console.ReadLine();
            continue;
        }
      
        Console.WriteLine("Выберите операцию: +, -, /, *");
        string sign = (Console.ReadLine());

        switch (sign)
        {
            case "+":
                Console.WriteLine($"Результат: {operand1} {sign} {operand2} = {operand1 + operand2}");
                break;

            case "-":
                Console.WriteLine($"Результат: {operand1} {sign} {operand2} = {operand1 - operand2}");
                break;

            case "*":
                Console.WriteLine($"Результат: {operand1} {sign} {operand2} = {operand1 * operand2}");
                break;

            case "/":
                if (operand2 == 0)
                {
                Console.WriteLine("Результат: на ноль делить нельзя");
                }
                else
                {
                Console.WriteLine($"Результат: {operand1} {sign} {operand2} = {operand1 / operand2}");
                }
                break;
            default:
                Console.WriteLine("Результат: неизвестная операция");
                break;
        }
        Console.ReadLine();
        
        }

    }
}